<?php
$errors = array();
function checkValid($id, $name, $age, $phone_number)
{
    global $errors;
    if (empty($_POST['id'])) {
        $errors['id'] = 'Please enter your id!';
    } else {
        $id = isset($_POST['id']) && is_numeric($_POST['id']) ? input($_POST['id']) : NULL;
    }
    if (empty($_POST['name'])) {
        $errors['name'] = 'Please enter your full name!';
    } else {
        $name = isset($_POST['name']) && isNameValid($_POST['name']) ? input($_POST['name']) : '';
    }
    if (empty($_POST['age'])) {
        $errors['age'] = 'Please enter your age!';
    } else {
        $age = isset($_POST['age']) && isAgeValid($_POST['age']) ? input($_POST['age']) : '';
    }
    if (empty($_POST['phone_number'])) {
        $errors['phone_number'] = 'Please enter a valid phone number!';
    } else {
        $phone_number = isset($_POST['phone_number']) && isPhoneValid($_POST['phone_number']) ? input($_POST['phone_number']) : '';
    }
    return array($id, $name, $age, $phone_number);
}

function isAgeValid($age)
{
    global $errors;
    if ($age >= 16 && $age <= 100) {
        return true;
    }
    $errors['age'] = "Invalid age";
    return false;
}

function isNameValid($name)
{
    global $errors;
    $pattern = '/([!@#$%^&*()_+~<>:"{}[\]?\/\\.,;\']+)/';
    if (preg_match($pattern, $name)) {
        $errors['name'] = 'Invalid name!';
        return false;
    }
    return true;
}

function isPhoneValid($phone_number)
{
    global $errors;
    $pattern = '/^[0-9\-\s]*$/';
    if (preg_match($pattern, $phone_number)) {
        return true;
    }
    $errors['phone_number'] = "Invalid phone number!";
    return false;
}

function input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = strip_tags($data);
    $data = htmlspecialchars($data);
    return $data;
}