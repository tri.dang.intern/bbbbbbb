<?php
session_start();
include_once '../model/Student.php';
$db = new MyPDO('phpcrud');
$db_world = new MyPDO('worlddata');
$pagination = new Pagination($db, 'student');

//CityState
$countryStateCity = new CountryStateCity($db_world);
$countryResult = $countryStateCity->getAllCountry();
//CityState

//Pagination
$records_per_page = 5;
$query = "select s.id,s.name,age,avatar,gender,address,email,phone_number,concat(c.country_name,'-',st.state_name,'-',ci.city_name) as location 
        from student s
        left join worlddata.country c on c.id=s.country_id
        left join worlddata.states st on st.id = s.state_id
        left join worlddata.city ci on ci.id = s.city_id
        order by s.id LIMIT ?, ?";
$allStudents = $pagination->getLimit($query,$records_per_page);
//Pagination
?>
<?php template_header('Main Table'); ?>
    <div style="display: none" class="msg" id="hiddenMsg">
        <?php if (isset($_SESSION['errorLog'])) : ?>
            <?php echo($_SESSION['errorLog']) ?>
            <?php unset($_SESSION['errorLog']) ?>
        <?php endif ?>
    </div>
    <div class="content read">
        <h2>Students List</h2>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createModal">Create
            Student
        </button>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="selectSearchType">Options</label>
                    </div>
                    <select class="custom-select" id="selectSearchType">
                        <option selected disabled>Select search type...</option>
                        <option value="id">Id</option>
                        <option value="name">Name</option>
                        <option value="age">Age</option>
                        <option value="phone_number">Phone number</option>
                    </select>
                </div>
            </div>
            <input type="text" name="search_text" id="search_text" placeholder="Search by Student Details..."
                   class="form-control"/>
        </div>
        <table class="table table-striped table-hover" id="mainTable">
            <thead>
            <tr >
                <td>#</td>
                <td>Avatar</td>
                <td>Name</td>
                <td>Age</td>
                <td>Phone Number</td>
                <td>Gender</td>
                <td>Address</td>
                <td>Email</td>
                <td>Location</td>
                <td class="text-center">Action</td>
            </tr>

            </thead>
            <tbody id="result">
            </tbody>
            <tbody id="mainTbody">
            <?php foreach ($allStudents as $student): ?>
                <tr>
                    <td><?= $student['id'] ?></td>
                    <?php if (isset($student['avatar'])) : ?>
                        <td>
                            <img src="../assets/images/upload/<?= $student['avatar'] ?>" alt="avatar"/>
                        </td>
                    <?php else: ?>
                    <td></td>
                    <?php endif; ?>
                    <td><?= $student['name'] ?></td>
                    <td><?= $student['age'] ?></td>
                    <td><?= $student['phone_number'] ?></td>
                    <td><?= $student['gender'] ?></td>
                    <td><?= $student['address'] ?></td>
                    <td><?= $student['email'] ?></td>
                    <td><?= $student['location'] ?></td>
                    <td class="actions">
                        <button type="button" class="btn btn-success editBtn">Edit&nbsp<i class="fas fa-pen"></i>
                        </button>
                        |
                        <button type="button" class="btn btn-danger deleteBtn">Delete&nbsp<i class="fas fa-trash"></i>
                        </button>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="pagination">
            <?php pagination($records_per_page) ?>
        </div>
    </div>
    <!--#####################CREATE MODAL(BOOTSTRAP)#######################-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createModalTitle">Create Record</h5>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form  enctype="multipart/form-data" action="../controller/insert.php" method="post"
                              id="createForm">
                            <span class="requireSym">* Required field</span>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="createId">ID<span class="requireSym">*</span></label>
                                    <input type="text" class="form-control" id="createId" name="id" placeholder="01">
                                </div>
                                <div class="form-group col-md-6 ml-auto">
                                    <label for="createName">Name<span class="requireSym">*</span></label>
                                    <input type="text" class="form-control" id="createName" name="name"
                                           placeholder="John Doe">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="createAge">Age<span class="requireSym">*</span></label>
                                    <input type="text" class="form-control" id="createAge" name="age" placeholder="18">
                                </div>
                                <div class="form-group col-md-6 ml-auto">
                                    <label for="createPhone">Phone Number<span class="requireSym">*</span></label>
                                    <input type="text" class="form-control" id="createPhone" name="phone_number"
                                           placeholder="0930603892">
                                </div>
                            </div>
                            <div class="form-group pr-md-1">
                                <label for="createAddress">Address</label>
                                <input type="text" class="form-control" id="createAddress" name="address"
                                       placeholder="1234 Main St">
                            </div>
                            <div class="form-group pr-md-1">
                                <label for="createEmail">Email</label>
                                <input type="email" class="form-control" id="createEmail" name="email"
                                       placeholder="example@mail.com">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="create-country-list">Country</label>
                                    <select id="create-country-list" class="form-control" name="country_id">
                                        <option value selected disabled>Select...</option>
                                        <?php foreach ($countryResult as $country) : ?>
                                            <option value="<?php echo $country['id']; ?>"><?php echo $country['country_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="create-state-list">State</label>
                                    <select id="create-state-list" class="form-control" name="state_id">
                                        <option value selected disabled>Select...</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="create-city-list">City</label>
                                    <select id="create-city-list" class="form-control" name="city_id">
                                        <option value selected disabled>Select...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <label class="form-check-label" for="createGenderM">Male</label>
                                    <input class="form-check-inline" value="Male" type="radio" name="gender"
                                           id="createGenderM">
                                    <label class="form-check-label" for="createGenderF">Female</label>
                                    <input class="form-check-inline" value="Female" type="radio" name="gender"
                                           id="createGenderF">
                                    <label class="form-check-label" for="createGenderOT">Other</label>
                                    <input class="form-check-inline" value="Other" type="radio" name="gender"
                                           id="createGenderOT"
                                           checked>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="createAvatar">Choose an avatar</label>
                                <input type="file" class="form-control-file" id="createAvatar" name="avatar"
                                       accept="image/jpg,image/jpeg,image/png,image/gif">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="createBtn" class="btn btn-primary">Create
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--####################CREATE MODAL(BOOTSTRAP)######################-->

    <!--####################DELETE MODAL(BOOTSTRAP)######################-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalTitle">Delete Record</h5>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form action="../controller/delete.php" method="post">
                            <input type="hidden" name="delete_id" id="delete_id">
                            <p>Do you want to delete this record?</p>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="deleteBtn" id="deleteBtn" class="btn btn-primary">Delete
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--####################DELETE MODAL(BOOTSTRAP)######################-->

    <!--####################UPDATE MODAL(BOOTSTRAP)######################-->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalTitle">Edit Record</h5>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form action="../controller/edit.php" method="post" id="editForm">
                            <span class="requireSym">* Required field</span>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="editId">ID<span class="requireSym">*</span></label>
                                    <input type="text" class="form-control" id="editId" name="id">
                                </div>
                                <div class="form-group col-md-6 ml-auto">
                                    <label for="editName">Name<span class="requireSym">*</span></label>
                                    <input type="text" class="form-control" id="editName" name="name"
                                           placeholder="John Doe">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="editAge">Age<span class="requireSym">*</span></label>
                                    <input type="text" class="form-control" id="editAge" name="age" placeholder="18">
                                </div>
                                <div class="form-group col-md-6 ml-auto">
                                    <label for="editPhone">Phone Number<span class="requireSym">*</span></label>
                                    <input type="text" class="form-control" id="editPhone" name="phone_number"
                                           placeholder="093060389">
                                </div>
                            </div>
                            <div class="form-group pr-md-1">
                                <label for="editAddress">Address</label>
                                <input type="text" class="form-control" id="editAddress" name="address"
                                       placeholder="1234 Main St">
                            </div>
                            <div class="form-group pr-md-1">
                                <label for="editEmail">Email</label>
                                <input type="email" class="form-control" id="editEmail" name="email"
                                       placeholder="example@mail.com">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="edit-country-list">Country</label>
                                    <select id="edit-country-list" class="form-control" name="country_id">
                                        <option value selected disabled>Select country...</option>
                                        <?php foreach ($countryResult as $country) : ?>
                                            <option value="<?php echo $country['id']; ?>"><?php echo $country['country_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="edit-state-list">State</label>
                                    <select id="edit-state-list" class="form-control" name="state_id">
                                        <option value selected disabled>Select...</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="edit-city-list">City</label>
                                    <select id="edit-city-list" class="form-control" name="city_id">
                                        <option value selected disabled>Select...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <label class="form-check-label" for="editGenderM">Male</label>
                                    <input class="form-check-inline" value="Male" type="radio" name="gender"
                                           id="editGenderM">
                                    <label class="form-check-label" for="editGenderF">Female</label>
                                    <input class="form-check-inline" value="Female" type="radio" name="gender"
                                           id="editGenderF">
                                    <label class="form-check-label" for="editGenderOT">Other</label>
                                    <input class="form-check-inline" value="Other" type="radio" name="gender"
                                           id="editGenderOT">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="editAvatar">Choose an avatar</label>
                                <input type="file" class="form-control-file" id="editAvatar" name="avatard"
                                       accept="image/jpg,image/jpeg,image/png,image/gif">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="editBtn" class="btn btn-primary">Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--####################UPDATE MODAL(BOOTSTRAP)######################-->
<?php template_footer(); ?>