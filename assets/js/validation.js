//#########VALIDATE CREATE FORM##########
$(document).ready(function () {
    $("form").each(function () {
        $(this).validate({
            errorClass: "invalidMsg",
            rules: {
                id: {
                    required: true,
                },
                name: {
                    required: true,
                    minlength: 8,
                    maxlength: 20,
                },
                age: {
                    required: true,
                },
                phone_number: {
                    required: true
                },
            },
            messages: {
                name: {
                    minlength: "Your name must be at least 8 characters"
                },
            },
            highlight: function (element) {
                $(element).css("background", '#ffdddd');
            },
            unhighlight: function (element) {
                $(element).css("background", '#ffffff');
            },
            submitHandler: function (form) {
                form.submit();
                localStorage.setItem("show", "true");
            },
        });
    });
});

$(document).ready(function () {
    var show = localStorage.getItem("show");
    if (show === "true") {
        $("#hiddenMsg").css('display', 'block');
        localStorage.setItem("show", "false");
    } else if (show === "false") {
        $("#hiddenMsg").css('display', 'none');
    }
});
//#########VALIDATE CREATE FORM##########

//############RESTRICT FIELD#############
(function ($) {
    $.fn.inputRestrict = function (inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    };
}(jQuery));
$("#editId").inputRestrict(function (value) {
    return /^\d*$/.test(value); //Integer => 0
});
$("#editName").inputRestrict(function (value) {
    return /^[ a-zA-Z0-9]{0,50}$/.test(value); //A-Z
});
$("#editAge").inputRestrict(function (value) {
    return /^\d{0,2}$/.test(value); //Integer => 0
});
$("#editPhone").inputRestrict(function (value) {
    return /^\d{0,10}$/.test(value); //Integer => 0
});
$("#editAddress").inputRestrict(function (value) {
    return /^[ a-zA-Z0-9]{0,100}$/.test(value); //A-Z
});
$("#editEmail").inputRestrict(function (value) {
    return /^[a-zA-Z0-9@.]{0,100}$/.test(value); //A-Z
});
$("#createId").inputRestrict(function (value) {
    return /^\d*$/.test(value); //Integer => 0
});
$("#createName").inputRestrict(function (value) {
    return /^[ a-zA-Z0-9]{0,50}$/.test(value); //A-Z
});
$("#createAge").inputRestrict(function (value) {
    return /^\d{0,2}$/.test(value); //Integer => 0
});
$("#createPhone").inputRestrict(function (value) {
    return /^\d{0,10}$/.test(value); //Integer => 0
});
$("#createAddress").inputRestrict(function (value) {
    return /^[ a-zA-Z0-9]{0,100}$/.test(value); //A-Z
});
$("#createEmail").inputRestrict(function (value) {
    return /^[a-zA-Z0-9@.]{0,100}$/.test(value); //A-Z
});
// $("#floatTextBox").inputFilter(function (value) {
//     return /^-?\d*[.,]?\d*$/.test(value); //Float
// });
// $("#currencyTextBox").inputFilter(function (value) {
//     return /^-?\d*[.,]?\d{0,2}$/.test(value); //at most 2 decimal places
// });
// $("#latinTextBox").inputFilter(function (value) {
//     return /^[a-z]*$/i.test(value); //A-Z
// });
//############RESTRICT FIELD#############