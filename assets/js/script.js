//########REDIRECT COUNTDOWN#############
function countdown() {
    var i = document.getElementById("counter");
    if (parseInt(i.innerHTML) <= 0) {
        location.href = "data.php";
    }
    if (parseInt(i.innerHTML) !== 0) {
        i.innerHTML = parseInt(i.innerHTML) - 1;
    }
}

setInterval(function () {
    // countdown();
}, 1000);
//########REDIRECT COUNTDOWN#############

//################Live Search############
$(document).ready(function () {
    load_data();

    function load_data(query, val) {
        $.ajax({
            url: "../controller/fetch.php",
            method: "POST",
            data: {
                query: query,
                val: val
            },
            success: function (data) {
                $('#result').html(data);
            }
        });
    }

    $("#search_text").keyup(function () {
        var search = $("#search_text").val();
        var val = $("#selectSearchType").val();
        if (search !== "") {
            load_data(search, val);
            $("#mainTbody").hide();
        } else {
            load_data();
            $("#mainTbody").show();
        }
    });

    $('#selectSearchType').on("change", function () {
        var val = $(this).val();
        $.ajax({
            success: function (data) {
                $("#search_text").attr("placeholder", "Search by " + val);
                return data;
            }
        });
    });
});
//################Live Search############

//###########Edit Delete Form############
$(document).ready(function () {
    $(".editBtn").on("click", function () {
        $("#editModal").modal('show');
        var tr = $(this).closest("tr");
        var data = tr.children("td").map(function () {
            return $(this).text();
        }).get();
        $("#editId").val(data[0]);
        $("#editName").val(data[2]);
        $("#editAge").val(data[3]);
        $("#editPhone").val(data[4]);
        if (data[5]=="Male"){
            $("#editGenderM").attr("checked","checked");
        }else if(data[5]=="Female"){
            $("#editGenderF").attr("checked","checked");
        }else {
            $("#editGenderOT").attr("checked","checked");
        }
        $("#editAddress").val(data[6]);
        $("#editEmail").val(data[7]);
    });
});

$(document).ready(function () {
    $(".deleteBtn").on("click", function () {
        $("#deleteModal").modal('show');
        var tr = $(this).closest("tr");
        var data = tr.children("td").map(function () {
            return $(this).text();
        }).get();
        console.log(data)
        $("#delete_id").val(data[0]);
    });
});
//###########Edit Delete Form############

//###########Get City State##############
$("#edit-country-list").change(function () {
    var val = $(this).val();
    $.ajax({
        type: "POST",
        url: "../model/CountryStateCity.php",
        data: "country_id=" + val,
        success: function (data) {
            $("#edit-state-list").html(data);
        }
    });
});
$("#edit-state-list").change(function () {
    var val = $(this).val();
    $.ajax({
        type: "POST",
        url: "../model/CountryStateCity.php",
        data: "state_id=" + val,
        success: function (data) {
            $("#edit-city-list").html(data);
        }
    });
});
$("#create-country-list").change(function () {
    var val = $(this).val();
    $.ajax({
        type: "POST",
        url: "../model/CountryStateCity.php",
        data: "country_id=" + val,
        success: function (data) {
            $("#create-state-list").html(data);
        }
    });
});
$("#create-state-list").change(function () {
    var val = $(this).val();
    $.ajax({
        type: "POST",
        url: "../model/CountryStateCity.php",
        data: "state_id=" + val,
        success: function (data) {
            $("#create-city-list").html(data);
        }
    });
});
//###########Get City State##############