<?php
include_once __DIR__ . '/MyPDO.php';
$db_world = new MyPDO('worlddata');

class CountryStateCity
{
    private $db, $data;

    public function __construct(MyPDO $db)
    {
        $this->db = $db;
    }

    public function getStateByCountryId($countryId)
    {
        $this->data = $this->db->run('SELECT * FROM worlddata.states WHERE country_id=?', [$countryId])->fetchAll();
        return $this->data;
    }

    public function getCityByStateId($stateId)
    {
        $this->data = $this->db->run('SELECT * FROM worlddata.city WHERE state_id=?', [$stateId])->fetchAll();
        return $this->data;
    }

    public function getAllCountry()
    {
        $this->data = $this->db->run('SELECT * FROM worlddata.country')->fetchAll();
        return $this->data;
    }
}

if (!empty($_POST['state_id'])) {
    $stateId = $_POST['state_id'];
    $countryStateCity = new CountryStateCity($db_world);
    $cityResult = $countryStateCity->getCityByStateId($stateId);
    ?>
    <option value selected disabled>Select City</option>
    <?php
    foreach ($cityResult as $city) {
        ?>
        <option value="<?php echo $city['id']; ?>"><?php echo $city['city_name']; ?></option>
        <?php
    }
}

if (!empty($_POST['country_id'])) {
    $countryId = $_POST['country_id'];
    $countryStateCity = new CountryStateCity($db_world);
    $stateResult = $countryStateCity->getStateByCountryId($countryId);
    ?>
    <option value selected disabled>Select State</option>
    <?php foreach ($stateResult as $state) { ?>
        <option value="<?php echo $state['id']; ?>"><?php echo $state['state_name']; ?></option>
        <?php
    }
}