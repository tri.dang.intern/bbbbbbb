<?php

class Pagination
{
    private $db, $data, $table;

    public function __construct(MyPDO $db, $table)
    {
        $this->table = $table;
        $this->db = $db;
    }

    public function setTotalRecord()
    {
        $this->data = $this->db->run("SELECT COUNT(*) FROM $this->table")->fetchColumn();
        return $this->data;
    }

    public function currentPage()
    {
        return isset($_GET['page']) ? (int)$_GET['page'] : 1;
    }

    public function totalPages($limit)
    {
        return ceil($this->setTotalRecord() / $limit);
    }


    public function getLimit($query, $records_per_page)
    {
        $this->data = $this->db->run($query, [($this->currentPage() - 1) * $records_per_page, $records_per_page])->fetchAll();
        return $this->data;
    }
}

function pagination($records_per_page)
{
    $db = new MyPDO('phpcrud');
    $pagination = new Pagination($db, 'student');
    $page = $pagination->currentPage();
    $total_record = $pagination->setTotalRecord();
    $total_pages = $pagination->totalPages($records_per_page);
    $tmp = [];
    for ($p = 1, $i = 0; $i < $total_record; $p++, $i += $records_per_page) {
        if ($page == $p) {
            // current page shown as bold, no link
            $tmp[] = "<a>{$p}</a>";
        } else {
            $tmp[] = "<a href=\"?page={$p}\">{$p}</a>";
        }
    }

    // thin out the links (optional)
    for ($i = count($tmp) - 2; $i > 1; $i--) {
        if (abs($page - $i - 1) > 2) {
            unset($tmp[$i]);
        }
    }

    // display page navigation iff data covers more than one page
    if (count($tmp) > 1) {
        echo '<p>';
        echo '<a href="?page=1">First</a>';
        if ($page > 1) {
            // display 'Prev' link
            echo "<a href=\"?page=" . ($page - 1) . "\">&laquo; Prev</a> |";
        }
        $lastLink = 0;
        foreach ($tmp as $i => $link) {
            if ($i > $lastLink + 1) {
                echo ' ... '; // where one or more links have been omitted
            } elseif ($i) {
                echo ' | ';
            }
            echo $link;
            $lastLink = $i;
        }

        if ($page <= $lastLink) {
            // display 'Next' link
            echo " | <a href=\"?page=" . ($page + 1) . "\">Next &raquo;</a>";
        }
        echo '<a href="?page=' . $total_pages . '">Last</a>';
        echo '</p>';
    }
}
