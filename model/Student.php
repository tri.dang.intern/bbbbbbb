<?php
include_once '../validation/validate_form.php';
include_once __DIR__ . '/Pagination.php';
include_once __DIR__ . '/CountryStateCity.php';
include_once __DIR__ . '/MyPDO.php';

class Student
{
    private $db;
    private $data;

    public function __construct(MyPDO $db)
    {
        $this->db = $db;
    }

    public function getColumnName()
    {
        $this->data = $this->db->run('DESCRIBE phpcrud.student')->fetchAll();
        return $this->data;
    }

    public function insert()
    {
        $allowed = array();
        $columnName = $this->getColumnName();
        foreach ($columnName as $key) {
            array_push($allowed, $key['Field']);
        }
        $params = [];
        $setStr = $setVal = '';
        foreach ($allowed as $key) {
            if (isset($_POST[$key])) {
                $setStr .= "$key,";
                $params[$key] = $_POST[$key];
            }
        }
        foreach ($allowed as $key) {
            if (isset($_POST[$key])) {
                $setVal .= ':' . "$key,";
                $params[$key] = $_POST[$key];
            }
        }
        $setStr = rtrim($setStr, ',');
        $setVal = rtrim($setVal, ',');
        $avatar = $_FILES['avatar']['name'];
        $this->db->run("INSERT INTO phpcrud.student($setStr,avatar) VALUES($setVal,'$avatar')", $params);
    }

    public function update()
    {
        $allowed = array();
        $columnName = $this->getColumnName();
        foreach ($columnName as $key) {
            array_push($allowed, $key['Field']);
        }
        $params = [];
        $setStr = '';
        foreach ($allowed as $key) {
            if (isset($_POST[$key]) && $key != 'id' && $key != 'avatar') {
                $setStr .= "`$key` = :$key,";
                $params[$key] = isset($_POST[$key]) ? $_POST[$key] : null;
            }
        }
        $setStr = rtrim($setStr, ',');
        $params['id'] = $_POST['id'];
        $this->db->run("UPDATE phpcrud.student SET $setStr WHERE id = :id", $params);
    }

    public function delete($id)
    {
        $this->db->run('DELETE FROM phpcrud.student WHERE id = ?', [$id]);
    }
}