<?php

class MyPDO extends PDO
{
    protected $db;

    public function __construct($database, $username = 'root', $password = 'root', $options = [])
    {
        $dsn = "mysql:host=localhost;dbname=$database;charset=utf8";
        try {
            $default_options = [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ];
            $options = array_replace($default_options, $options);
            parent::__construct($dsn, $username, $password, $options);
        } catch (PDOException $e) {
            echo "Database could not be connected: " . $e->getMessage();
        }
    }

    public function __destruct()
    {
        $this->db = null;
    }

    public function run($sql, $args = NULL)
    {
        try {
            if (!$args) {
                return $this->query($sql);
            }
            $stmt = $this->prepare($sql);
            $stmt->execute($args);
            echo $sql . '<br>';
            print_r($args);
            return $stmt;
        } catch (PDOException $e) {
            echo '<pre>';
            echo $sql;
            echo '</pre>';
            echo $e->getMessage();
        }
    }
}

function countRedirect()
{
    echo <<<DMT
    <p>You will be redirected in <span id="counter">5</span> second(s).</p>
DMT;
}

function template_header($title)
{
    echo <<<EOT
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>$title</title>
		<link href="/PdoCRUD/assets/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	</head>
	<body>
    <nav class="navtop">
    	<div>
    		<h1>STUDENT DATABASE</h1>
            <a href="/PdoCRUD/index.php"><i class="fas fa-home"></i>Home</a>
    		<a href="/PdoCRUD/view/data.php"><i class="fas fa-address-book"></i>Students</a>
    	</div>
    </nav>
EOT;
}

function template_footer()
{
    echo <<<EOT
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="/PdoCRUD/assets/js/script.js" type="text/javascript"></script>
    <script src="/PdoCRUD/assets/js/validation.js" type="text/javascript"></script>
    </body>
</html>
EOT;
}
