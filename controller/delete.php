<?php
session_start();
include_once '../model/Student.php';
$db = new MyPDO('phpcrud');
$student = new Student($db);

if (isset($_POST['deleteBtn'])) {
    $student->delete($_POST['delete_id']);
    $_SESSION['errorLog'] = 'Record have been deleted!';
    header('Location:../view/data.php?page=1');
} else {
    $_SESSION['errorLog'] = 'Failed to delete record!';
    header('Location:../view/data.php?page=1');
}
exit;