<?php
session_start();
include_once '../model/Student.php';
$db = new MyPDO('phpcrud');
$student = new Student($db);

if (isset($_POST['createBtn'])) {
    $student->insert();
    $uploaddir = '../assets/images/upload/';
    $uploadfile = $uploaddir . basename($_FILES['avatar']['name']);
    echo "<p>";
    if (move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadfile)) {
        echo "File is valid, and was successfully uploaded.\n";
    } else {
        echo "Upload failed";
    }
    echo "</p>";
    echo '<pre>';
    echo 'Here is some more debugging info:';
    print_r($_FILES);
    print "</pre>";
    $_SESSION['errorLog'] = 'Record created successfully!';
//        header('Location:data.php');
} else {
    $_SESSION['errorLog'] = 'Failed to create record!';
//    header('Location:data.php');
}
exit;