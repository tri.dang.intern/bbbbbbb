<?php
include_once '../model/Student.php';
$db = new MyPDO('phpcrud');

if (isset($_POST['query'])) {
    $search_string = $_POST['query'];
    $searchCol = !empty($_POST['val']) ? $_POST['val'] : 'id';
    $regex_first = '\\b';
    $regex_second = '\\b';
    $str = $regex_first . $search_string . $regex_second;
    $query = "SELECT *FROM phpcrud.student WHERE $searchCol REGEXP ?";
    $results = $db->run($query,[$str]);
    while ($row = $results->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr>' .
            '<td>' . $row['id'] . '</td>' .
            '<td>' . $row['name'] . '</td>' .
            '<td>' . $row['age'] . '</td>' .
            '<td>' . $row['phone_number'] . '</td>' .
            '<td class="actions">' .
            '<a  class="edit" href="edit.php?id=' . $row['id'] . '">' .
            '<i class="fas fa-pen fa-xs"></i>' .
            '</a>' .
            '<a class="trash" href="delete.php?id=' . $row['id'] . '">' .
            '<i class="fas fa-trash fa-xs"></i>' .
            '</a>' .
            '</td>' .
            '</tr>';
    }
    if ($results->rowCount() < 1) {
        echo '<td>' . 'No Data Found!' . '</td>';
    }
}
